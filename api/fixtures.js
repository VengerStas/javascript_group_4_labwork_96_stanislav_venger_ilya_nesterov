const mongoose = require('mongoose');
const nanoid = require('nanoid');

const config = require('./config');

const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for ( let collection of collections) {
    await collection.drop();
  }

  await User.create(
    {
      username: "admin_hvgwmrx_charlie@tfbnw.net",
      facebookId: "102368607679439", role: 'admin',
      token: nanoid(), password: '3SvuNH4Xnz6apT2',
      avatar: 'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=102368607679439&height=50&width=50&ext=1561227249&hash=AeQRI_qogOTCFSD0',
      displayName: "Admin Charlie"
    }
  );

  return connection.close();
};

run().catch(error => {
  console.log(error)
});