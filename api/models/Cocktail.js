const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const IngredientSchema = new Schema ({
  ingredient: {type: String},
  amount: {type: String}
});

const RatingsSchema = new Schema ({
  idUser: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  rating: {type: Number}
});

const CocktailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  cocktail: {type: String},
  image: {type: String},
  recipe: {type: String},
  published: {
    type: Boolean,
    required: true,
    default: false
  },
  ingredients: [IngredientSchema],
  ratings: [RatingsSchema]
});


const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;