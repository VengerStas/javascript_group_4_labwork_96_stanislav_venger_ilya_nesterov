const User = require('../models/User');

const tryAuth = async (req, res, next) => {
  const token = await req.get("Authorization");

  if(!token) return next();

  const user = await User.findOne({token});

  if(!user && !user.role) return next();

  req.user = user;


  next();
};

module.exports = tryAuth;