const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');
const config = require("../config");
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');

const Cocktail = require('../models/Cocktail');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathCocktail);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
  try {
    let criteria = {published: true};

    if (req.user) {
      criteria = {
        $or: [
          {published: true},
          {user: req.user._id}
        ]
      }
    }
    if (req.user && req.user.role === 'admin') {
      criteria = {
        $or: [
          {published: true},
          {published: false},
          {user: req.user._id}
        ]
      }
    }

    const cocktail = await Cocktail.find(criteria).populate('user', '_id, displayName');
    res.send(cocktail);
  } catch(e) {
    if(e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(e)
  }
});

router.get('/:id', async (req, res) => {
  const cocktailId = req.params.id;
  try {
    const cocktail = await Cocktail.findById(cocktailId).populate('user', '_id, displayName');
    res.send(cocktail);
  } catch(e) {
    if(e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(e)
  }
});

router.post('/', [auth, upload.single('image')], (req, res) => {
  const cocktailInfo = {
    user: req.user._id,
    cocktail: req.body.cocktail,
    recipe: req.body.recipe,
    ingredients: JSON.parse(req.body.ingredients)
  };

  if (req.file) {
    cocktailInfo.image = req.file.filename;
  }

  const cocktail = new Cocktail(cocktailInfo);

  cocktail.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error))
});

router.post('/:id/rating', [auth], async (req, res) => {
  const cocktail = await Cocktail.findById(req.params.id);

  if(!cocktail) {
    return res.sendStatus(404);
  }

  let rating = {...req.body};
  cocktail.ratings.push(rating);

  await cocktail.save();

  return res.send({message: 'Ваш голос учтен'});
});

router.post('/:id/publish', [auth, permit('admin')], async (req, res) => {
  const cocktail = await Cocktail.findById(req.params.id);

  if(!cocktail) {
    return res.sendStatus(404);
  }

  cocktail.published = !cocktail.published;

  await cocktail.save();

  return res.send(cocktail);
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
  Cocktail.remove({_id: req.params.id})
      .then(result => res.send(result))
      .catch(error => res.status(403).send(error))
});

module.exports = router;
