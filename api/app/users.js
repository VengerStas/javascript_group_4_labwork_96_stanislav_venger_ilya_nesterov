const express = require('express');
const config = require("../config");
const axios = require('axios');
const nanoid = require('nanoid');

const User = require('../models/User');

const router = express.Router();

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Logged out'};

  if(!token) {
    return res.send(success);
  }
  const user = await User.findOne({token});

  if(!user) {
    return res.send(success);
  }

  user.generateToken();
  await user.save();

  return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
  const inputToken = req.body.accessToken;
  const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);

    if (response.data.data.error) {
      return res.status(401).send({message: 'Facebook token incorrect'});
    }

    if (response.data.data.user_id !== req.body.id) {
      return res.status(401).send({message: 'Wrong user ID'});
    }

    let user = await User.findOne({facebookId: req.body.id});

    if (!user) {
      user = new User ({
        username: req.body.email || req.body.id,
        displayName: req.body.name,
        avatar: req.body.picture.data.url,
        password: nanoid(),
        facebookId: req.body.id
      });
    }

    user.generateToken();

    await user.save();

    return res.send({message: 'Login registered', user});

  } catch (error){
    return res.status(401).send({message: 'Facebook token incorrect'});
  }
});


module.exports = router;
