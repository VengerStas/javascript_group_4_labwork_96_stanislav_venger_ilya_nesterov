const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPathAvatar: path.join(rootPath, 'public/uploads/avatar'),
  uploadPathCocktail: path.join(rootPath, 'public/uploads/cocktail'),
  dbUrl: 'mongodb://localhost/cocktail',  //название обязательно
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '676209732799999', //обязательно
    appSecret: '9bf4827d5ef87f3f204e72481b651a04' // обязательно
  }
};