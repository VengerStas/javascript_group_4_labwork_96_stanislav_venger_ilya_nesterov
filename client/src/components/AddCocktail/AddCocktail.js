import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

import FormElement from "../UI/Form/FormElement";
import {postCocktail} from "../../store/actions/cocktailActions";

import './AddCocktail.css';

class AddCocktail extends Component {

  state = {
    cocktail: '',
    image: null,
    recipe: '',
    ingredients: [{ingredient: '', amount: ''}]
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    const ingredients = JSON.stringify(this.state.ingredients);

    formData.append('cocktail', this.state.cocktail);
    formData.append('image', this.state.image);
    formData.append('recipe', this.state.recipe);
    formData.append('ingredients', ingredients);

    this.props.postCocktail(formData)
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };

  handleAddIngredient = () => {
    this.setState({
      ingredients: this.state.ingredients.concat([{ ingredient: "" , amount: ""}])
    });
  };

  handleIngredientChange = index => event => {
    const newIngredients = this.state.ingredients.map((ingredient, ingredientIndex) => {
      if (index !== ingredientIndex) return ingredient;
      return { ...ingredient, [event.target.name]: event.target.value};
    });

    this.setState({ ingredients: newIngredients});
  };

  handleRemoveIngredient = index => () => {
    this.setState({
      ingredients: this.state.ingredients.filter((s, ingredientIndex) => index !== ingredientIndex)
    });
  };


  render() {
    return (
      <Fragment>
        <Form onSubmit={this.submitFormHandler}>
          <FormElement
            value={this.state.cocktail}
            onChange={this.inputChangeHandler}
            type="text" title='Cocktail'
            propertyName='cocktail'
            placeholder='Enter cocktail name'
          />

          {this.state.ingredients.map((ingredient, index) => (
              <div key={index}>
                <FormGroup row>
                  <Label for="ingredients" sm={2}>Ingredient #{index + 1}</Label>
                  <Col sm={7}>
                    <Input
                      type="text" name="ingredient"
                      id="ingredients"
                      placeholder={`Add ingredient #${index + 1} name`}
                      value={ingredient.ingredient}
                      onChange={this.handleIngredientChange(index)}
                    />
                  </Col>

                  <Col sm={2}>
                    <Input
                      type="text" name="amount"
                      id="amount"
                      placeholder={`Amount #${index + 1}`}
                      value={ingredient.amount}
                      onChange={this.handleIngredientChange(index)}
                    />
                  </Col>

                    {
                        index > 0 &&
                        <Col sm={1}>
                            <Button color="danger" className='ingredient' outline onClick={this.handleRemoveIngredient(index)}>
                                <i className="far fa-trash-alt"/>
                            </Button>
                        </Col>
                    }
                </FormGroup>
              </div>
            )
          )}

          <FormGroup row>
            <Col sm={{offset:2, size: 10}}>
              <Button outline color='success' onClick={this.handleAddIngredient}>Add ingredient</Button>
            </Col>
          </FormGroup>

          <FormElement
            value={this.state.recipe}
            onChange={this.inputChangeHandler}
            type="textarea" title='Recipe'
            propertyName='recipe'
            placeholder='Enter recipe'
          />

          <FormElement
            propertyName="image"
            title="Image cocktail"
            type="file"
            onChange={this.fileChangeHandler}
          />

          <FormGroup row>
            <Col sm={{offset:2, size: 10}}>
              <Button type="submit" color="primary">Save</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  postCocktail: cocktailData => dispatch(postCocktail(cocktailData))
});

export default connect(null, mapDispatchToProps)(AddCocktail);