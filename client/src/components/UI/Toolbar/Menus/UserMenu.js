import React, {Fragment} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';

const UserMenu = ({user, logout}) => (
  <UncontrolledDropdown nav inNavbar>
    <DropdownToggle nav caret>
      Hello,
      <img src={user.avatar} className='userAvatar' alt='user'/>
       {user.displayName}
    </DropdownToggle>
    <DropdownMenu right>
      {user && user.role === 'admin' ? null
        : <Fragment>
          <DropdownItem>
            <NavLink tag={RouterNavLink} to="/add_cocktail">Add cocktails</NavLink>
          </DropdownItem>
          <DropdownItem>
            <NavLink tag={RouterNavLink} to="/my_cocktails">My cocktails</NavLink>
          </DropdownItem>
          <DropdownItem divider />
        </Fragment>}
      <DropdownItem onClick={logout}>
        <NavLink tag={RouterNavLink} to="/">Logout</NavLink>
      </DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
);

export default UserMenu;