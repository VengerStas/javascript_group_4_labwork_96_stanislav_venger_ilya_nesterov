import React, {Fragment} from 'react';
import FacebookLogin from "../../../../components/FacebookLogin/FacebookLogin";

const AnonymousMenu = () => (
    <Fragment>
      <FacebookLogin/>
    </Fragment>
);

export default AnonymousMenu;