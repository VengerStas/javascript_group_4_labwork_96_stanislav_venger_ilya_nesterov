import React from 'react';
import {Route, Switch} from "react-router-dom";
import Cocktail from "./containers/Cocktail/Cocktail";
import UsersCocktails from "./containers/UsersCocktails/UsersCocktails";
import AddCocktail from "./components/AddCocktail/AddCocktail";
import CocktailInfo from "./containers/CocktailInfo/CocktailInfo";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Cocktail}/>
      <Route path="/cocktail_details/:id" exact component={CocktailInfo} />
      <Route path="/my_cocktails" exact component={UsersCocktails}/>
      <Route path="/add_cocktail" exact component={AddCocktail}/>
    </Switch>
  );
};

export default Routes;