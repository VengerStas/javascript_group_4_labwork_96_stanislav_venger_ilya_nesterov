import React, {Component} from 'react';
import {getCurrentCocktail, ratingCocktail} from "../../store/actions/cocktailActions";
import {connect} from "react-redux";
import ParamThumbnail from "../../components/ParamThumbnail/ParamThumbnail";
import {Card, CardBody, CardFooter, CardText, CardTitle, Col, Row} from "reactstrap";
import Spinner from "../../components/UI/Spinner/Spinner";

import './CocktailInfo.css';
import StarRatings from "react-star-ratings";


class CocktailInfo extends Component {
    componentDidMount() {
        this.props.getCurrentCocktail(this.props.match.params.id);
    }

    changeRating = (newRating, name) => {
        const rating = {
            rating: newRating,
            id: this.props.match.params.id,
            idUser: this.props.user._id
        };
        this.props.ratingCocktail(rating);
    };

    render() {
        if (!this.props.cocktails) return <Spinner/>;

        let ingredient = this.props.cocktails.ingredients.map(ingredient => {
           return (
               <li key={ingredient._id}><span>{ingredient.ingredient}</span><span>{ingredient.amount}</span></li>
           )
        });


        const rating = this.props.cocktails.ratings.map(rat => {
            return rat.rating;
        });
        let totalRating = 0;
        for (let i = 0; i < rating.length; i++) {
            totalRating += rating[i];
        }
        let resultRating = totalRating / rating.length;
        return (
                <Card>
                    <div className="cocktail-header">
                        <h4 className="header-title"><span>Title: </span>{this.props.cocktails.cocktail}</h4>
                        <span>
                            Rating:
                            {`${!rating.length ? (0 + ' ') : resultRating.toFixed(2)}`}
                            <i className="far fa-star" style={{color: 'gold'}}/>
                            {`  (${rating.length} voices)`}
                        </span>
                    </div>
                    <CardBody>
                        <Row>
                            <Col>
                                <ParamThumbnail param='cocktail' image={this.props.cocktails.image}/>
                            </Col>
                            <Col>
                                <CardTitle>
                                    <strong>Ingredients:</strong>
                                    <ul>
                                        {ingredient}
                                    </ul>
                                </CardTitle>
                                <CardText><strong>Recipe: </strong> {this.props.cocktails.recipe}</CardText>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <strong style={{paddingRight: '10px'}}>Rate:  </strong>
                      {!this.props.user ?
                        <strong>Авторизуйтесь чтобы проголосовать</strong> :
                        <StarRatings
                          rating={0}
                          changeRating={this.changeRating}
                          name='rating'
                          starDimension='20px'
                          starRatedColor='gold'
                        />
                      }
                    </CardFooter>
                </Card>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    cocktails: state.cocktail.cocktail
});

const mapDispatchToProps = dispatch => ({
    getCurrentCocktail: cocktailId => dispatch(getCurrentCocktail(cocktailId)),
    ratingCocktail: cocktailRating => dispatch(ratingCocktail(cocktailRating))
});

export default connect(mapStateToProps, mapDispatchToProps)(CocktailInfo);