import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {cocktailPublish, deleteCocktail, getCocktails} from "../../store/actions/cocktailActions";
import {Button, ButtonGroup, Card, CardBody, CardText, CardTitle, NavLink} from "reactstrap";
import ParamThumbnail from "../../components/ParamThumbnail/ParamThumbnail";
import {NavLink as RouterNavLink} from "react-router-dom";

import './Cocktail.css';

class Cocktail extends Component {
  componentDidMount() {
    this.props.getCocktails()
  }

  render() {
    return (
      <Fragment>
        <h4>Cocktails list</h4>
        <div className="cocktails-list">
          {
                this.props.cocktails.map(cocktail => {
                  return (
                      cocktail.published === true || (this.props.user && this.props.user.role === "admin") ?
                      <Card key={cocktail._id} className="cocktail-card">
                        <ParamThumbnail param="cocktail" image={cocktail.image}/>
                        <CardBody>
                          <CardTitle>{cocktail.cocktail}</CardTitle>
                          <NavLink tag={RouterNavLink} to={'/cocktail_details/' + cocktail._id}>Read more</NavLink>
                          <CardText className="text-right">
                            {cocktail.published === false ? <strong>Not publish</strong> : <span>Опубликовал: <strong>{cocktail.user.displayName}</strong></span>}
                          </CardText>
                          {
                            this.props.user && this.props.user.role === "admin" &&
                                <ButtonGroup>
                                  <Button color="success" onClick={() => this.props.cocktailPublish(cocktail._id)}>
                                    {cocktail.published === true ? 'Unpublish' : 'Publish'}
                                  </Button>
                                  <Button color="danger" onClick={() => this.props.deleteCocktail(cocktail._id)}>Delete</Button>
                                </ButtonGroup>
                          }
                        </CardBody>
                      </Card> : null
                  )
                })
          }
        </div>
      </Fragment>

    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  cocktails: state.cocktail.cocktails
});

const mapDispatchToProps = dispatch => ({
  getCocktails: () => dispatch(getCocktails()),
  deleteCocktail: cocktailId => dispatch(deleteCocktail(cocktailId)),
  cocktailPublish: cocktail => dispatch(cocktailPublish(cocktail))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktail);