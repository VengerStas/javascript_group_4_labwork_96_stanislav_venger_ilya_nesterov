import React, {Component} from 'react';
import {connect} from "react-redux"
import {getCocktails} from "../../store/actions/cocktailActions";
import ParamThumbnail from "../../components/ParamThumbnail/ParamThumbnail";
import {Alert, Card, CardBody, CardText, CardTitle, NavLink} from "reactstrap";
import {NavLink as RouterNavLink, Redirect} from "react-router-dom";

import './UsersCockails.css';

class UsersCocktails extends Component {
  componentDidMount() {
    this.props.getCocktails();
  }

  render() {
    if (!this.props.user) return <Redirect to='/' />;
    return (
      <div className="my-cocktails-block">
        <h4>My cocktails</h4>
        <div className="cocktails-list">
          {this.props.cocktail.map(cocktail => {
            return (
              this.props.user._id === cocktail.user._id ?
                  <Card key={cocktail._id} className="cocktail-card">
                    <ParamThumbnail param="cocktail" image={cocktail.image}/>
                    <CardBody>
                      <CardTitle>{cocktail.cocktail}</CardTitle>
                      <NavLink tag={RouterNavLink} to={'/cocktail_details/' + cocktail._id}>Read more</NavLink>
                      {
                        cocktail.published === true ?
                          <Alert color="success">
                            Your cocktail is published
                          </Alert> :
                          <Alert color="danger">
                            Your cocktail is not published
                          </Alert>
                      }
                      <CardText className="text-right">
                        {cocktail.published === false ? <strong>Not publish</strong> : <span>Опубликовал: <strong>{cocktail.user.displayName}</strong></span>}
                      </CardText>
                    </CardBody>
                  </Card>
              : null
            )
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  cocktail: state.cocktail.cocktails
});

const mapDispatchToProps = dispatch => ({
  getCocktails: () => dispatch(getCocktails())
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersCocktails);