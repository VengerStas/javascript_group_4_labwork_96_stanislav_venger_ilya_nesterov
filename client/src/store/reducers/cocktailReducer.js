import {FETCH_COCKTAIL_SUCCESS, FETCH_COCKTAILS_SUCCESS, LOADING_COCKTAIL} from "../actions/cocktailActions";

const initialState = {
  cocktails: [],
  cocktail: null,
  loading: true
};

const cocktailReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COCKTAILS_SUCCESS:
      return {...state, cocktails: action.cocktails};
    case FETCH_COCKTAIL_SUCCESS:
      return {...state, cocktail: action.cocktail};
    case LOADING_COCKTAIL:
      return {...state, loading: action.cancel};
    default:
      return state;
  }
};

export default cocktailReducer;