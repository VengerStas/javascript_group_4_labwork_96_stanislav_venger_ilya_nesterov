import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";

export const FETCH_COCKTAILS_SUCCESS = 'FETCH_COCKTAILS_SUCCESS';
export const FETCH_COCKTAIL_SUCCESS = 'FETCH_COCKTAIL_SUCCESS';
export const LOADING_COCKTAIL = 'LOADING_COCKTAIL';
export const RATING_COCKTAIL = 'RATING_COCKTAIL';

const cocktailRatingUser = rating => ({type: RATING_COCKTAIL, rating});

const fetchCocktailsSuccess = cocktails => ({type: FETCH_COCKTAILS_SUCCESS, cocktails});
const fetchCocktailSuccess = cocktail => ({type: FETCH_COCKTAIL_SUCCESS, cocktail});

const loadingCocktail = cancel => ({type: LOADING_COCKTAIL, cancel});

export const getCocktails = () => {
  return dispatch => {
    return axios.get('/cocktail').then(
      response => {
        dispatch(fetchCocktailsSuccess(response.data));
      }
    ).finally(
      () => dispatch(loadingCocktail(false))
    );
  }
};

export const getCurrentCocktail = cocktailId => {
    return dispatch => {
        return axios.get('/cocktail/' + cocktailId).then(
            response => {
                dispatch(fetchCocktailSuccess(response.data))
            }
        ).finally(
            () => dispatch(loadingCocktail(false))
        );
    }
};

export const ratingCocktail = cocktailRating => {
  return dispatch => {
    const rating = {idUser: cocktailRating.idUser, rating: cocktailRating.rating};
    return axios.post(`/cocktail/${cocktailRating.id}/rating`, rating).then(
      response => {
        dispatch(getCurrentCocktail(cocktailRating.id));
        dispatch(cocktailRatingUser(response.data))
      }
    )
  }
};

export const postCocktail = cocktailData => {
  return dispatch => {
    return axios.post('/cocktail', cocktailData).then(
      () => {
        dispatch(getCocktails());
        NotificationManager.info('Cocktail created, but not published!');
        dispatch(push('/my_cocktails'))
      }
    )
  }
};

export const cocktailPublish = cocktail => {
    return (dispatch) => {
        return axios.post(`/cocktail/${cocktail}/publish`).then((response) => {
            NotificationManager.success(response.data.published === true ? 'Cocktail has been published' : 'Cocktail has been Unpublished');
            dispatch(getCocktails());
            dispatch(push('/'));
        });
    }
};

export const deleteCocktail = cocktailId => {
    return (dispatch) => {
        return axios.delete('/cocktail/' + cocktailId).then(() => {
            NotificationManager.success('Cocktail has been deleted');
            dispatch(getCocktails());
            dispatch(push('/'));
        })
    }
};