import {push} from 'connected-react-router';
import {NotificationManager}  from 'react-notifications';
import axios from '../../axios-api';
import {getCocktails} from "./cocktailActions";

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const config = {headers: {'Authorization': token}};

    return axios.delete('/users/sessions', config).then(
      () => {
        dispatch({type: LOGOUT_USER});
        NotificationManager.success('You are logged!');
        dispatch(push('/'));
      },
      error => {
        NotificationManager.error('Could not logged!')
      }
    )
  }
};

export const facebookLogin = userData => {
  return dispatch => {
    return axios.post('/users/facebookLogin', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data.user));
        NotificationManager.success('Logged in via facebook');
        dispatch(push('/'));
        dispatch(getCocktails())
      },
      () => {
        dispatch(loginUserFailure('Login via Facebook failed'))
      }
    )
  }
};
